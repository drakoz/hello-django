from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render

def hello(request):
    # return HttpResponse('Hello World with GitLab CI!')
    return render(request, 'index.html')
